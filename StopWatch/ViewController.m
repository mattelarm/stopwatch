//
//  ViewController.m
//  StopWatch
//
//  Created by IT-Högskolan on 2015-04-19.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *TimeLabel;
@property (strong, nonatomic) NSTimer *stopWatchTimer;
@property (strong, nonatomic) NSDate *startDate;
@property (weak, nonatomic) IBOutlet UIButton *startPause;

@end

@implementation ViewController
- (IBAction)StartButton:(id)sender {
    
    if (running == NO ) {
        running = YES;
        self.stopWatchTimer = [NSTimer scheduledTimerWithTimeInterval:1.0/10.0 target:self selector:@selector(UpdateTimer:) userInfo:nil repeats:YES];
        self.startDate = [[NSDate date] init];
        [sender setTitle:@"Pause" forState:UIControlStateNormal];
        
    }
    else {
        running = NO;
        secondsRun += [[NSDate date]timeIntervalSinceDate:_startDate];
        [self.stopWatchTimer invalidate];
        _stopWatchTimer = nil;
        [sender setTitle:@"Start" forState:UIControlStateNormal];
    }

}

- (IBAction)StopButton:(id)sender {
    secondsRun += [[NSDate date] timeIntervalSinceDate:_startDate];
    [self.stopWatchTimer invalidate];
    self.stopWatchTimer = nil;
    secondsRun = 0;
    _TimeLabel.text = @"00:00.0";
    
}

-(void)UpdateTimer:(NSTimer *)tim {
    NSDate *currentDate = [NSDate date];
    
    NSTimeInterval timeInterval = [currentDate timeIntervalSinceDate:_startDate];
    
    timeInterval += secondsRun;
    NSDate *timerDate = [NSDate dateWithTimeIntervalSince1970:timeInterval];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"mm:ss.S"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0.0]];
    
    NSString *timeString = [dateFormatter stringFromDate:timerDate];
    self.TimeLabel.text = timeString;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
   
    running = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
